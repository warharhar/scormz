import { Injectable } from '@angular/core';
import * as _ from 'underscore';
import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';


@Pipe ({
    name:'customerIdFilter'
})
@Injectable({
  providedIn: 'root'
})
export class ScormService {
  constructor(private http:HttpClient) { }

  getRecent(){
    let url = `https://dev-scormz-api.brainlitz.com/api/recentcourseList`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
  }

  getCourseList(skip,limit,keyword){
    return this.http.get("HTTPS://192.168.1.172:3222/api/courseList?limit=&skip=&keyword");
  }

  getTotalCourse(){
    let url = `HTTPS://dev-scormz-api.brainlitz.com/api/coursecount`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
  }

  getNumOfRegisteration(){
    let url = `HTTPS://dev-scormz-api.brainlitz.com/api/learnercount`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
  }

  getAction(startdate,enddate){
    let url = `https://dev-scormz-api.brainlitz.com/api/lists?startdate=${startdate}&enddate=${enddate}`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
  }

  transform(items: any[], field: string, value: string): any[] {
    if (!items) {
      return [];
    }
    if (!field || !value) {
      return items;
    }

    return items.filter(singleItem =>
      singleItem[field].toLowerCase().includes(value.toLowerCase())
    );
  }
  addCourse(courseInfo){
    return this.http.post("https://dev-scormz-api.brainlitz.com/api/importCourse",courseInfo);
  }

  getList(page,amount){
    let url = `https://dev-scormz-api.brainlitz.com/api/courseList?page=${page}&amount=${amount}`
    // let urlSearch='https://dev-scormz-api.brainlitz.com/api/courseList?search=${search}'
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
    return this.http.get(url, httpOptions).map((res: Response) => {
      let result = res;
      console.log(result);
      return result;
    })
  }

  getListSearch(search){
    let urlSearch=`https://dev-scormz-api.brainlitz.com/api/courseList?search=${search}`;
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
    return this.http.get(urlSearch, httpOptions).map((res: Response) => {
      let result = res;
      console.log(result);
      return result;
    })
  }

  //delete course
  delete(courseid:any){
    let url='this.http.delete(`https://dev-scormz-api.brainlitz.com/api/deleteCourse/`+courseid)';
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
    
}

getCourseVersion(courseId){
 let url = `https://dev-scormz-api.brainlitz.com/api/viewCourse?courseId=${courseId}`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
}

getCourseDetail(courseId,version){
 let url = `https://dev-scormz-api.brainlitz.com/api/viewCourse?courseId=${courseId}&version=${version}`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
}

getTotalLearner(courseId,startdate,enddate,version){
  let url = `HTTPS://dev-scormz-api.brainlitz.com/api/learnerTotal?courseId=${courseId}&startdate=${startdate}&enddate=${enddate}&version=${version}`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
}

getCourseHeatmap(courseId,startdate,enddate,version){
 let url = `https://dev-scormz-api.brainlitz.com/api/pageUsageDetails?courseId=${courseId}&startdate=${startdate}&enddate=${enddate}&version=${version}`
    const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Apikey': "f4dad53d720e5082b03a366bbfa25334",
      'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
    })
  };
  return this.http.get(url, httpOptions).map((res: Response) => {
    let result = res;
    console.log(result);
    return result;
  });
}

getVersion(){
  return this.http.get(`HTTPS://dev-scormz-api.brainlitz.com/api/courseList?`);
}

getUserDetail(learnerId,courseId,startdate,enddate){
let url = `HTTPS://dev-scormz-api.brainlitz.com/api/userDetails/${learnerId}?courseId=${courseId}&startdate=${startdate}&enddate=${enddate}`
const httpOptions = {
headers: new HttpHeaders({
  'Content-Type': 'application/json',
  'Apikey': "f4dad53d720e5082b03a366bbfa25334",
  'secret_key': "nEPmoP2Nz7PWy4GV8uSXtOvJdHaGIgAaGegBbno1"
})
};
return this.http.get(url, httpOptions).map((res: Response) => {
let result = res;
console.log(result);
return result;
});
}

}

