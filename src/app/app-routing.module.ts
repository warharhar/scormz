import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CourselistComponent } from './courselist/courselist.component';
import { CoursedetailComponent } from './coursedetail/coursedetail.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { DashboradComponent } from './dashborad/dashborad.component';


const routes: Routes = [
  {path:'courselist',component:CourselistComponent},
  {path:'coursedetail',component:CoursedetailComponent},
  {path:'dashboard',component:DashboradComponent},
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
