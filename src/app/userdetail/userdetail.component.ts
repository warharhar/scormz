import { Component, OnInit } from '@angular/core';
import { TabsetConfig } from 'ngx-bootstrap/tabs';
import { HttpClient } from '@angular/common/http';
import { ScormService } from '../scorm.service';
import * as d3 from '../d3.js';
declare var $:any;
export function getTabsetConfig(): TabsetConfig {
  return Object.assign(new TabsetConfig(), { type: 'pills' });
}

@Component({
  selector: 'app-userdetail',
  templateUrl: './userdetail.component.html',
  styleUrls: ['./userdetail.component.css'],
  providers: [{ provide: TabsetConfig, useFactory: getTabsetConfig }]
})
export class UserdetailComponent implements OnInit {
  LearnerDetail:any=[];
  courseDetail:any=[];
  totalLearner:any=[];
  

  constructor(private http:HttpClient,private service:ScormService) { }

  public getLocalstorage(){
   let test,test1,test2,test3,test4;
   test=localStorage.getItem('courseId');
   test1=localStorage.getItem('learnerId');
   test2=localStorage.getItem('startdate');
   test3=localStorage.getItem('enddate');
   test4=localStorage.getItem('version');
   console.log(test);
   console.log(test1);
   console.log(test2);
   console.log(test3); 
   this.getUserDetail(test1,test,test2,test3);
 }

  getUserDetail(learnerId,courseId,startdate,enddate){
       this.service.getUserDetail(learnerId,courseId,startdate,enddate).subscribe(res=>{
     this.LearnerDetail=res;
     console.log(this.LearnerDetail);
     
     })    
  }

  ngOnInit() {
    this.getLocalstorage();      
  }

}
