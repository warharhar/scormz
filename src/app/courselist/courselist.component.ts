import { Component, OnInit,ElementRef, Input  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ScormService } from '../scorm.service';
import { Pipe, PipeTransform } from '@angular/core';
// import {InfiniteScrollDirective} from 'src/directive/directive';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {RequestOptions, Headers, Http } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';

@Component({
  selector: 'app-courselist',
  templateUrl: './courselist.component.html',
  styleUrls: ['./courselist.component.css']
})

@Pipe({
  name: 'filter'
})
export class CourselistComponent implements OnInit {
  Loading: boolean=true;
  searchText:boolean=false;
  constructor(private http:HttpClient,private service:ScormService,private router:Router,private el: ElementRef) { }
  transform(items: any[], searchText: string): any[] {
    if(!items) return [];
    if(!searchText) return items;
searchText = searchText.toLowerCase();
return items.filter( it => {
      return it.toLowerCase().includes(searchText);
    }); 
   }
 skip:"number";
 limit:"number";
users:any=[];
api:any=[];
course:any=[];
count:any=1;
searchCourse="";
temp:any;
tt:any;
Launchcourse:any=[];

deleteMethod(name: any) {
  this.deleteCourse(""+name);  
  
}

deleteCourse(courseid){
  this.service.delete(courseid).subscribe(res=>{
    console.log(courseid);
    location.reload();
  })
  
}

Launch(Courseid: any){
  console.log(Courseid);  
   return this.http.get(`HTTPS://dev-scormz-api.brainlitz.com/api/launchCourse?courseId=${Courseid}&learnerId=dc396148-9669-11e9-bc42-526af7764f64`).subscribe(res=>{
    this.Launchcourse = res;
    console.log(this.Launchcourse);
    
  })   
}
// to call list of courses
getList(page,amount,ss){
  if(ss===null){
    return this.service.getList(page,amount).subscribe(res=>{
      this.course = this.course.concat(res);
    },err=>{
      console.log(err);
    })
  }
  else{
    return this.service.getListSearch(ss).subscribe(res=>{
      this.course=res;
      console.warn(res);
      
    })
  }
  // this.service.getList(page,amount,search).subscribe(res=>{
  //   this.course = this.course.concat(res);
  // })
}
// search(event){
//    let ss=event.tartet.value;
// }
loadMoreNext(){
  this.count+=1;
  this.getList(this.count,20,null);
 
}

// onUpload(){
//   this.addNewCourse();
// }
description: string;
addNewCourse() {
 var uploadInput: any = document.getElementById("CarouselFileUpload");
 var files = uploadInput.files;

 if (files.length > 0) {
 
 var url = 'HTTPS://dev-scormz-api.brainlitz.com/api/importCourse';
 var xhr = new XMLHttpRequest();
 var fd = new FormData();
 xhr.open("POST", url, true);
 fd.append('courseDescription',this.description);
 fd.append("courseFile", files[0]);
 xhr.send(fd);
 console.log(fd);
 }

 else {
 alert("please choose file");
 }

 }
 //Edit Course

data:any={"courseId":"","version":""};
coursedata={"courseId":"","courseTitle":"","createDate":"","description":"","registerationcount":"","version":""};

editCourse(description,version){
   
   this.coursedata.description=description;
   this.coursedata.version=version;
}

version:string;
courseDescription:string;
courseId:string;
editNewImport(courseid){
  // this.http.put('https://dev-scormz-api.brainlitz.com/api/editCourse/'+courseid,this.coursedata).subscribe(res=>{
  //   console.log(res);
  // });
  var uploadInput: any = document.getElementById("newImport");
  var files = uploadInput.files;

  if (files.length > 0) {
  var url = 'https://dev-scormz-api.brainlitz.com/api/editCourse/'+courseid;
  var xhr = new XMLHttpRequest();
  var fd = new FormData();
  xhr.open("PUT", url, true);
  fd.append('courseId',this.courseId);
  fd.append('version',"");
  fd.append('courseDescription',this.courseDescription);
  fd.append("courseFile", files[0]);
  xhr.send(fd);
}
}

ngOnInit() {
  this.temp=this.getList(1,20,null);
}

}
