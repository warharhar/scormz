FROM node:10.15.0

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
COPY package*.json /usr/src/app/
RUN npm install
RUN npm install --save-dev mini-css-extract-plugin

RUN npm install -g @angular/cli@1.6.5
COPY . /usr/src/app
CMD ng serve --host 0.0.0.0
EXPOSE 4200