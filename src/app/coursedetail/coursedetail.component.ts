import { Component, OnInit, AfterContentInit,HostListener } from '@angular/core';
import {NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter, NgbDatepicker} from '@ng-bootstrap/ng-bootstrap';
import * as d3 from '../d3.js';
import * as moment from "moment";
import { HttpClient } from '@angular/common/http';
import { ScormService } from '../scorm.service';
import { InfiniteScroll } from 'angular2-infinite-scroll';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {RequestOptions} from '@angular/http';


declare var d3: any;
declare var $:any;

// declare var heatmap:any;
@Component({
  selector: 'app-coursedetail',
  templateUrl: './coursedetail.component.html',
  styleUrls: ['./coursedetail.component.css']
})

export class CoursedetailComponent implements OnInit {
  bookHeatMapLoading: boolean = true;
  isOffline: boolean = false;
  public sub: any;
  public mode: string;
  public isAllZero: boolean = false;
  public getVal: any;
  learnerTotal:boolean=false;

 courseDetail:any=[];
 public startDate;
 public endDate;
 public start_date;
public end_date;
public count=1;
public learnerId;
courseid:any;
versionName:any;
data:any=[];
courseName:any=[];
totalLearner:any=[];
courseVersion:any=[];

constructor(private http:HttpClient,private service:ScormService,private route:Router) {}
selectCourse(event:any){
  this.courseid=event.target.value;
  console.log(this.courseid);
  console.log(this.start_date);
  console.log(this.end_date);
  if(this.courseid!=null&&this.startDate!=null && this.endDate!=null){
    this.getCourseVersion(this.courseid);
  }
  if(this.courseid!=null&&this.startDate!=null && this.endDate!=null && this.versionName!=null){
    this.getCourseDetail(this.courseid,this.versionName);
    this.getCourseHeatMap(this.courseid, this.start_date,this.end_date,this.versionName);
    this.getTotalLearner(this.courseid, this.start_date,this.end_date,this.versionName);
   
  }
 }
 
  selectVersion(event:any){
   this.versionName=event.target.value;
   console.log(this.courseid);
   console.log(this.start_date);
   console.log(this.end_date);
   console.log(this.versionName);
   if(this.startDate!=null && this.endDate!=null&& this.courseid!=null){
     this.getCourseDetail(this.courseid,this.versionName);
     this.getCourseHeatMap(this.courseid, this.start_date,this.end_date,this.versionName);
     this.getTotalLearner(this.courseid, this.start_date,this.end_date,this.versionName);
   } 
  }

onDateSelect(e,type){
  
  this.toIsoString(e,type)
  if(this.startDate!=undefined && this.endDate!=undefined){
  this.service.getAction(this.start_date,this.end_date).subscribe(res=>{
    console.warn(this.start_date);
    this.courseName=res;
  })
  
  }
 
if(this.startDate!=null&& this.endDate!=null&&this.courseid!=null&&this.versionName!=null)
    {
      this.getCourseDetail(this.courseid,this.versionName);
      this.getCourseHeatMap(this.courseid, this.start_date,this.end_date,this.versionName);
      this.getTotalLearner(this.courseid, this.start_date,this.end_date,this.versionName);
    }
}

getCourseVersion(courseId){
  
  this.service.getCourseVersion(courseId).subscribe(res=>{
    this.courseVersion=res;
  })
}

getCourseDetail(courseId,version){  
    this.service.getCourseDetail(courseId,version).subscribe(res=>{
    this.courseDetail=res;
  })
}

getTotalLearner(courseId,startdate,enddate,version){  
    return this.service.getTotalLearner(courseId,startdate,enddate,version).subscribe(res=>{ 
    this.totalLearner=res;   
}
    )   
} 

heatMapData;
getCourseHeatMap(courseId,startdate,enddate,version): any{
 
    this.service.getCourseHeatmap(courseId,startdate,enddate,version)
      .subscribe((res: any) =>{
        console.log(res);
        
        this.isOffline = false;
        this.heatMapData = res
        this.bookHeatMapLoading = false;
        this.drawHeatMap(res);
      
    })
    
      }
@HostListener('mouseover', ['$event']) mouseOver($event) {
  const container = document.getElementById('tooltip')
  if ($event.path[2].id==='heatmap-svg') {
    console.warn($event);
    console.warn(container);
    container.style.left = $event.clientX+'px';
    container.style.top = $event.clientY+'px';
  }
}
drawHeatMap(data: any) {
  const margin = {  top: 30, right: 0, bottom: 30, left: 30 },
        width = 850 - margin.left - margin.right,
        height = 432 - margin.top - margin.bottom,
        gridSize = Math.floor(width / 40),
        gridSizenew = Math.floor(width / 15),
        legendElementWidth = gridSize * 2,
        buckets = 5,
        colors = ['#FBE9E7', '#FFCCBC', '#FFAB91', '#FF8A65', '#FF7043', '#FF5722'],
        // tslint:disable-next-line: max-line-length
        times = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'],
        days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  const svg = d3.select('#svg-g');

  const dayLabels = svg.selectAll('.dayLabel')
      .data(days)
      .enter().append('text')
        .text(function (d) { return d; })
        .attr('y', 0)
        .attr('x', function (d, i) { return i * gridSizenew; })
        .attr('style', 'text-anchor:end; font-size:14px; font-weight: bolder;')
        .attr('transform', 'translate(' + gridSizenew / 1.3 + '-25)')
        .attr('class', function (d, i) { return ((i >= 0 && i <= 4) ? 'dayLabel mono axis axis-workweek' : 'dayLabel mono axis'); });

  const timeLabels = svg.selectAll('.timeLabel')
      .data(times)
      .enter().append('text')
        .text(function(d) { return d; })
        .attr('y', function(d, i) { return i * gridSize; })
        .attr('x', 0)
        .attr('style', 'text-anchor:middle; font-size:14px')
        .attr('transform', 'translate(-30,' + gridSize / 1.6 + ')')
        .attr('class', function(d, i) { return ((i >= 7 && i <= 10) ? 'timeLabel mono axis axis-worktime' : 'timeLabel mono axis'); });
        var tooltip= d3.select('#chart').append('div')
        .style('opacity', 0)
        .attr('class', 'tooltip')
        .attr('id', 'tooltip')
        .style('background-color', '#ffab91')
        .style('color', 'white')
        .style('width', '100px')
        .style('height', '80px')
        .style('position', 'absolute')
        .style('border', 'solid')
        .style('border-width', '2px')
        .style('border-radius', '5px')
        .style('padding', '5px')

  const showTip = function(d) {
    console.warn($('#' + d.Hour + '-' + d.Day))
 
    // $('#' + d.Hour + '-' + d.Day).tooltip('show');
    tooltip.html('The exact value of<br>this cell is: ' + d.value + d.Hours + d.Day);
  };
  const hideTip = function(d) {
    // console.warn($('#' + d.Hour + '-' + d.Day))
    
    // $('#' + d.Hour + '-' + d.Day).tooltip('dispose');
  };
  var mouseover = function(d) {
    tooltip.style('opacity', 1)
  };
  let mousemove = function(d) {
    tooltip
      .html('Day:'+ d.Day+'<br>'+'Hour:'+ d.Hour+'<br>'+'Value:'+ d.value);
      // .style('left', left+ 'px')
      // .style('top', top + 'px')
      // tooltip.style('left', (d3.mouse(this)[0]+70) + 'px')
      // tooltip.style('top', (d3.mouse(this)[1]) + 'px')
  }
  const mouseleave = function(d) {
    tooltip.style('opacity', 0)
  }
  const heatmapChart = function(res) {
      const data = res;
      // console.warn(d3)
      const colorScale = d3.scaleQuantile()
          .domain([0, (d3.max(data, function(d) {return d.value; }) / 2), d3.max(data, function(d) {return d.value; })])
          .range(colors);
    console.error(colorScale())
      const cards = svg.selectAll('.hour')
          .data(data, function(d) {return d.Day + ':' + d.Hour; });

      cards.append('title');

      cards.enter().append('rect')
          .attr('y', function(d) { return (d.Hour-1) * gridSize; })
          .attr('x', function(d) { return (d.Day - 1) * gridSizenew; })
          .attr('rx', 0)
          .attr('ry', 0)
          .attr('class', 'hour bordered')
          .attr('style', 'stroke:#999; stroke-width:1px; stroke-opacity:0.3')
          .attr('width', gridSizenew)
          .attr('height', gridSize)
          .attr('data-toggle', 'tooltip')
          .attr('data-placement', 'top')
          .attr('data-html', true)
          .attr('title', function(d) { return `Day: ${d.Day} <br> Hour: ${d.Hour} <br> Value: ${d.value}`; })
          .attr('id', function(d) { return d.Hour + '-' + d.Day; })
          .style('fill',function(d) { return colorScale(d.value) } )
          .on('mouseover', function(d) {mouseover(d); })
          .on('mousemove', function(d) {mousemove(d); })
          .on('mouseleave', function(d) {mouseleave(d); })

      cards.transition().duration(1000)
          .style('fill', function(d) {
            console.error(colorScale(d.value))
            return colorScale(d.value);
          });


      cards.select('title').text(function(d) { return d.value; });

      cards.exit().remove();
  };
  heatmapChart(data);
}


    

toIsoString(date,type){
  console.warn(date);
  if (date.day < 10) {
    var day = '0' + `${date.day}`;
    } else {
    var day = `${date.day}`;
    }
    
    if (date.month < 10) {
    var month = '0' + `${date.month}`;
    } else {
    var month = `${date.month}`;
    }
    var testing =
    `${date.year}` + '-' + `${month}` + '-' + `${day}`;
    // const zz= moment(new Date(testing)).format();
    var isoDate = new Date(testing).toISOString();
    if(type==='start'){
      this.start_date = isoDate
    }else{
      this.end_date = isoDate
    }
}

public setLocalStorage(id:any){
  this.learnerId=id;
  console.log(id);
  localStorage.setItem('courseId',this.courseid);
  localStorage.setItem('startdate',this.start_date);
  localStorage.setItem('enddate',this.end_date);
  localStorage.setItem('learnerId',this.learnerId);
  localStorage.setItem('version',this.versionName);
  // localStorage.setItem('startDate',this.startDate);
  // localStorage.setItem('endDate',this.endDate);
  
  
  console.log(this.courseid);
  console.log(this.start_date);
  console.log(this.end_date);
  console.log(this.learnerId);
  console.warn(this.versionName);
  
}

showUserDetails = false;
  test(){
    this.showUserDetails = true;
  }
  backtoCourseDetail(){
    this.showUserDetails = false;
    console.warn(this.heatMapData);
    setTimeout(() => {
      this.drawHeatMap(this.heatMapData)
    }, 100);
  }
  ngOnInit() {

    
}
}


