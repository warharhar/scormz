import { Component, OnInit } from '@angular/core';
import { ScormService } from '../scorm.service';

@Component({
  selector: 'app-dashborad',
  templateUrl: './dashborad.component.html',
  styleUrls: ['./dashborad.component.css']
})
export class DashboradComponent implements OnInit {
  busy: Promise<any>;
  constructor(private service:ScormService) { }
  course:any=[];
  coursecount:any=[];
  noregisteration:any=[];
 getRecentList(){
   this.service.getRecent().subscribe(res=>{
    this.course=res;
    console.log(this.course);
   })
 }

 
getCourseCount(){
  this.service.getTotalCourse().subscribe(res=>{
    this.coursecount=res;
    console.log(this.coursecount);
  })
}

getNumberOfregisteration(){
  this.service.getNumOfRegisteration().subscribe(res=>{
    this.noregisteration=res;
    console.log(this.noregisteration);
  })
}

//add new course
description: string;
 uploadCarouselImageClick() {
  
  var uploadInput: any = document.getElementById("CarouselFileUpload");
  var files = uploadInput.files;

  if (files.length > 0) {
  
  var url = 'HTTPS://dev-scormz-api.brainlitz.com/api/importCourse';
  var xhr = new XMLHttpRequest();
  var fd = new FormData();
  xhr.open("POST", url, true);
  fd.append('courseDescription',this.description);
  fd.append("courseFile", files[0]);
  xhr.send(fd);
  console.log(fd);
  }

  else {
  alert("please choose file");
  }

  }
  
  ngOnInit() {
    console.log('init');
    
    this.getRecentList();
    this.getCourseCount();
    this.getNumberOfregisteration();
  }

}
