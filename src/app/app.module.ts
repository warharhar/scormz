import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourselistComponent } from './courselist/courselist.component';
import { CoursedetailComponent } from './coursedetail/coursedetail.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { DashboradComponent } from './dashborad/dashborad.component';
import {HttpClientModule} from '@angular/common/http';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {NgxPaginationModule} from 'ngx-pagination';
// import { CalendarHeatmap } from 'angular2-calendar-heatmap';
// import {InfiniteScrollModule} from 'ngx-infinite-scroll';
// import {InfiniteScrollDirective} from 'src/directive/directive';
import {ScormService} from './scorm.service'
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { InfiniteScrollModule } from 'angular2-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
// import { HeatmapComponent } from './heatmap/heatmap.component';
import {BusyModule} from 'angular2-busy';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    CourselistComponent,
    CoursedetailComponent,
    UserdetailComponent,
    DashboradComponent,
    ScormService,
    
    
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    AngularDateTimePickerModule,
    NgbModule,
    TabsModule.forRoot(),
    Ng2SearchPipeModule,
    NgxPaginationModule,
    InfiniteScrollModule,
    BusyModule,
    BrowserAnimationsModule,

    
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
// platformBrowserDynamic().bootstrapModule(AppModule);
